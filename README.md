Two in One
----
The goal of this project is to build a video editing bot.

Secondary goal is to make a bot infrastructure. 

Like ErrBot but nicer. And only for telegram.


Alarm - traffic
----

Folder `/bin` contains ffmpeg binaries for Windows (188 MB).

Just to minimize the effort for none devs, who want to run it on there Windows machine.


Setup
----
All requirements are as always located in `requirements.txt`.
Install them with `pip` by running `pip install -r requirements.txt`

Ask @BotFather (telegram bot) to give you a bot token and save it in a `.env` file inside the root folder of the project.

File should look like this:
```dotenv
TG_TOKEN=XXXXXXXXX:AAAAA-AAAAAAAAAAAAAAAAA-AAAAAAAAAAA
```

If you have ffmpeg installed or you are on linux/mac - edit the `FFMPEG_PATH` 
path in `settings.py`. Or add `FFMPEG_PATH` env. var.  

Now run `python main.py`.

Your bot is ready.


Usage
----

- `/start` - prints list of available commands.
- `/g video_url start_seconds:end_seconds` - downloads the video, cuts a clip from start_seconds to end_seconds and sends it to you in telegram.


TODO
----

- ~~move download progress bar in it's own class~~
- run ffmpeg in another process
- show ffmpeg progress in chat
- make infra for easy multi-step commands 