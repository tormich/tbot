def start(bot, update):
    if update.effective_chat['type'] == 'private':
        available_commands = '\n'.join([f'/{i.command[0]}' for i in bot.dispatcher.handlers[0]])
        msg_lines = [f'Hello 👋',
                     f'available commands:',
                     available_commands]

        bot.send_message(text='\n'.join(msg_lines),
                         chat_id=update.message.chat_id)
