import requests
from telegram import InlineKeyboardMarkup, InlineKeyboardButton

from storage import mac_storage
from utils.routers import Dlink


def dlink(bot, update):
    langs_markup = InlineKeyboardMarkup([[
        InlineKeyboardButton('DHCP clients', callback_data='dlink_dhcpinfo'),
        InlineKeyboardButton('Public IP', callback_data='dlink_publicip'),
        InlineKeyboardButton("Share location", request_location=True, callback_data='dlink_location')

    ]])
    bot.send_message(
        text='Yob', reply_markup=langs_markup, chat_id=update.message.chat_id)


def process_dlink(bot, update):
    query = update.callback_query
    if query.data == 'dlink_dhcpinfo':
        dl = Dlink(admin_pass='')
        dl.login()
        hosts = '\n'.join(
            map(lambda x: f'👩‍💻 {x.ip} {x.name}\n{x.mac}\n{mac_storage.get(x.mac) or ""}\n', dl.dhcp_info()))
        text = f'DHCP clients:\n{hosts}'
    elif query.data == 'dlink_publicip':
        ip = requests.get('https://canihazip.com/s').text
        print(ip)
        text = f'Public IP\n{ip}\n Web ui http://{ip}:8080'
    else:
        text = 'Not Implemented'
    bot.edit_message_text(
        text=text, chat_id=query.message.chat_id,
        message_id=query.message.message_id,
        disable_web_page_preview=True)