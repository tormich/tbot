import hashlib
import os
import subprocess as sp
from functools import partial
from time import time
from typing import Callable

import youtube_dl

import settings
from bot import ProgressMessage


class DownloadProgress(ProgressMessage):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.msg_head = '🥔 Downloading'

    def on_update(self, event):
        if event['status'] == 'finished':
            self.update_msg(text='🍋 Downloaded')

        elif event['status'] == 'downloading':
            if time() - self.last_sent_time < 2:  # Avoid spamming telegram with messages
                return

            percent = float(event["_percent_str"][:-1].strip())
            self.update_progress(percent)


def download_video(video_url: str, out_path: str, progress_hook: Callable):
    """ https://unix.stackexchange.com/questions/230481/how-to-download-portion-of-video-with-youtube-dl-command
    """
    ydl_opts = {"outtmpl": out_path,
                "noplaylist": True,
                "ignoreerrors": False,
                "format": "bestvideo[ext=mp4]+bestaudio[ext=m4a]/best[ext=mp4]/best",
                "no_color": False}

    if progress_hook is not None:
        ydl_opts['progress_hooks'] = [progress_hook]

    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        # vinfo = ydl.extract_info(video_url, download=False)
        ydl.download([video_url])


def youtubedl(bot, update):
    _, video_url, timerange = update.message.text.split()

    info_msg = bot.send_message(text=f'🔎', chat_id=update.message.chat_id,
                                reply_to_message_id=update.message.message_id)
    update_info_msg = partial(bot.edit_message_text, chat_id=update.message.chat_id,
                              message_id=info_msg.result().message_id,
                              disable_web_page_preview=False)

    progress_msg = DownloadProgress(bot, chat_id=update.message.chat_id, message_id=info_msg.result().message_id)

    video_url_hash = hashlib.md5(video_url.encode()).digest().hex()
    download_path = os.path.join(settings.DOWNLOAD_FOLDER, f'dl-{video_url_hash}.mp4')

    if not os.path.isfile(download_path):
        try:
            download_video(video_url, download_path, progress_hook=progress_msg.on_update)
        except Exception as e:
            update_info_msg(text=str(e))
            return

    update_info_msg(text=f'🎬 ffmpeging')

    out_path = os.path.join(settings.VIDEO_FOLDER,
                            f'out-{video_url_hash}-{update.message.chat_id}-{update.message.message_id}.mp4')

    timerange = [int(x) for x in timerange.split(':')]

    result = sp.run([settings.FFMPEG_PATH,
            '-y',  # overwrite output files
            '-i', download_path,  # input path
            '-ss', str(timerange[0]), '-t', str(timerange[1] - timerange[0]),
            out_path])

    if result.returncode > 0:
        update_info_msg(text=f"🥺 ffmpeg returned code {result.returncode}")

    else:
        with open(out_path, 'rb') as f:
            update_info_msg(text='📯 Sending...')
            bot.send_video(chat_id=update.message.chat_id,
                           video=f,
                           timeout=120,
                           reply_to_message_id=update.message.message_id)

        bot.delete_message(chat_id=update.message.chat_id,
                           message_id=info_msg.result().message_id)


def youtubejustdl(bot, update):
    _, video_url = update.message.text.split()

    info_msg = bot.send_message(text=f'🔎', chat_id=update.message.chat_id,
                                reply_to_message_id=update.message.message_id)
    update_info_msg = partial(bot.edit_message_text, chat_id=update.message.chat_id,
                              message_id=info_msg.result().message_id,
                              disable_web_page_preview=False)

    progress_msg = DownloadProgress(bot, chat_id=update.message.chat_id, message_id=info_msg.result().message_id)

    video_url_hash = hashlib.md5(video_url.encode()).digest().hex()
    download_path = os.path.join(settings.DOWNLOAD_FOLDER, f'dl-{video_url_hash}.mp4')

    if not os.path.isfile(download_path):
        try:
            download_video(video_url, download_path, progress_hook=progress_msg.on_update)
        except Exception as e:
            update_info_msg(text=str(e))
            return
    with open(download_path, 'rb') as f:
        update_info_msg(text='📯 Sending...')
        bot.send_video(chat_id=update.message.chat_id,
                       video=f,
                       timeout=120,
                       # reply_to_message_id=update.message.message_id
                       )

    bot.delete_message(chat_id=update.message.chat_id,
                       message_id=info_msg.result().message_id)
