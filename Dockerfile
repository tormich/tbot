FROM python:3.8-alpine
ENV PYTHONUNBUFFERED 1

RUN mkdir /code
WORKDIR /core

RUN apk update && apk add gcc libc-dev make git libffi-dev openssl-dev python3-dev libxml2-dev libxslt-dev ffmpeg

# RUN apk add --no-cache --virtual .build-deps ffmpeg
# RUN apk add --no-cache --virtual linux-headers musl-dev python-dev
#     ca-certificates gcc postgresql-dev linux-headers musl-dev \
#     libffi-dev jpeg-dev zlib-dev

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
COPY . .

ENTRYPOINT python main.py
