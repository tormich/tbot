import os

from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv())


TG_TOKEN = os.environ['TG_TOKEN']

FFMPEG_PATH = os.environ.get('FFMPEG_PATH', r'ffmpeg')
DOWNLOAD_FOLDER = os.environ.get('DOWNLOAD_FOLDER', 'downloads')
VIDEO_FOLDER = os.environ.get('VIDEO_FOLDER', 'downloads')
