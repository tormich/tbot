from collections import namedtuple

import requests
import re


class Dlink:
    DHCPInfo = namedtuple('DHCPInfo', ['name', 'mac', 'ip', 'expires_in'])
    LOGIN_URL_TEMPLATE = 'http://{ip}/login.cgi?web_login_name={admin_name}&web_login_pass={admin_pass}&loginId=Login'

    def __init__(self, ip='192.168.1.1', admin_name='admin', admin_pass='admin'):
        self.ip = ip
        self.admin_name = admin_name
        self.admin_pass = admin_pass

    def login(self):
        requests.get(self.LOGIN_URL_TEMPLATE.format(ip=self.ip,
                                                    admin_name=self.admin_name,
                                                    admin_pass=self.admin_pass))

    def dhcp_info(self):
        html = requests.get(f'http://{self.ip}/dhcpinfo.html').text
        ptrn = r'(?P<h>[\w\d\-\_\ ]*)</\D*>(?P<m>[\w\d\:]*)</\D*>(?P<ip>[\d\.]*)</\D*>(?P<exp>[\w\d\ ,]*)</'

        for host in re.findall(ptrn, html):
            info = self.DHCPInfo(*host)
            if not info.mac:
                continue
            else:
                yield info


