from time import time

import youtube_dl


def emoji_progress_bar(percent: float, width=10, progress_symbol='🥕'):
    percent_per_symbol = 100/width
    progress = progress_symbol * int(percent / percent_per_symbol)
    return f'[{progress:➖<10}]'

#
# class Downloader:
#     def __init__(self, bot, chat_id):
#     def download_progress(bot):
#         moon_emojis = '🌑🌘🌗🌖🌕'
#         state = {'last_sent': 0.}
#
#         def callback(d):
#             if d['status'] == 'finished':
#                 # file_tuple = os.path.split(os.path.abspath(d['filename']))
#                 # print("Done downloading {}".format(file_tuple[1]))
#                 bot.edit_message_text(
#                     text=f'{moon_emojis[-1]} Downloaded ',
#                     chat_id=update.message.chat_id,
#                     message_id=msg.result().message_id,
#                     disable_web_page_preview=False)
#             if d['status'] == 'downloading':
#                 # print(d['filename'], d['_percent_str'], d['_eta_str'])
#                 percent = float(d["_percent_str"][:-2].strip())
#                 moon = moon_emojis[int(percent / (100 / (len(moon_emojis) - 1)))]
#                 if time() - state['last_sent'] < 2:
#                     return
#
#                 state['last_sent'] = time()
#                 progress = '🥕' * int(percent / 10)
#                 bot.edit_message_text(
#                     # text=f'{moon} Downloading {d["_percent_str"]}, {d["_eta_str"]}\n'
#                     text=(f'{moon} Downloading\n'
#                           f'[{progress:➖<10}] {d["_percent_str"]}'),
#                     chat_id=update.message.chat_id,
#                     message_id=msg.result().message_id,
#                     disable_web_page_preview=False)
#
#         return callback
#
#
#     def start(self, bot, update):
#         _, video_url, timerange = update.message.text.split()
#         out_fps = 30
#         start_frame, end_frame = map(lambda x: int(x) * out_fps, timerange.split(':'))
#         if abs(start_frame - end_frame) > 10 * out_fps:
#             bot.send_message(text=f'🦐 time range {start_frame} : {end_frame} is too big', chat_id=update.message.chat_id)
#             return
#
#         msg = bot.send_message(text=f'Working...', chat_id=update.message.chat_id)
#
#         last_msg_time = [0]
#
#         ydl_opts = {'outtmpl': f'downloads/dl-{update.message.chat_id}-{update.message.message_id}.mp4',
#                     'progress_hooks': [download_progress]}
#         with youtube_dl.YoutubeDL(ydl_opts) as ydl:
#             ydl.download([video_url])
#
#         bot.edit_message_text(
#             text=f'🎬 ffmpeging...',
#             chat_id=update.message.chat_id,
#             message_id=msg.result().message_id,
#             disable_web_page_preview=False)
#
#         out_path = f'downloads/out-{update.message.chat_id}-{update.message.message_id}.gif'
#         stream = (ffmpeg
#                   .input(ydl_opts['outtmpl'])
#                   .filter('fps', out_fps)
#                   .trim(start_frame=start_frame, end_frame=end_frame)
#                   .filter('scale', width=-1, height=260)
#                   .output(out_path))
#         ffmpeg.run(stream_spec=stream, cmd='bin\\ffmpeg.exe', overwrite_output=True)
#
#         os.remove(ydl_opts['outtmpl'])
#         with open(out_path, 'rb') as f:
#             # bot.send_animation(chat_id=update.message.chat_id,
#             #                    animation=f,
#             #                    timeout=120,
#             #                    reply_to_message_id=update.message.message_id)
#             bot.send_video(chat_id=update.message.chat_id,
#                            video=f,
#                            timeout=120,
#                            reply_to_message_id=update.message.message_id)
#
#         bot.edit_message_text(
#             text=f'🎬 Done',
#             chat_id=update.message.chat_id,
#             message_id=msg.result().message_id,
#             disable_web_page_preview=False)
