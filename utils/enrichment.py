import requests


def mac_lookup(mac: str) -> str:
    vendor = requests.get(f'https://api.macvendors.com/{mac}')
    return vendor.text


wasia_state = {
    "location": {},
    "mood": {},
    "is_hungry": False,
    "speed": [1.23, 0.3],
    "direction": {},
    "group": {}
}
