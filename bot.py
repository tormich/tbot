from collections import namedtuple
from functools import partial
from time import time

import telegram.bot
from telegram.ext import Updater, CommandHandler
from telegram.ext import messagequeue as mq
from telegram.utils.request import Request

Cmd = namedtuple('Cmd', ['command', 'func', 'callbacks'])


class MQBot(telegram.bot.Bot):
    """ A subclass of Bot which delegates send method handling to MQ """

    def __init__(self, *args, mqueue=None, **kwargs):
        super().__init__(*args, **kwargs)
        self._is_messages_queued_default = True
        self._msg_queue = mqueue or mq.MessageQueue()
        self.updater = Updater(bot=self)
        self.dispatcher = self.updater.dispatcher

    def __del__(self):
        self._msg_queue.stop()
        super().__del__()

    @mq.queuedmessage
    def send_message(self, *args, **kwargs):
        return super().send_message(*args, **kwargs)

    @mq.queuedmessage
    def edit_message_text(self, *args, **kwargs):
        return super().edit_message_text(*args, **kwargs)

    def register_commands(self, commands):
        for cmd in commands:
            command = CommandHandler(cmd.command, cmd.func)
            self.dispatcher.add_handler(command)
            if not cmd.callbacks:
                continue

            for callback in cmd.callbacks:
                self.updater.dispatcher.add_handler(callback)

    def start_polling(self):
        self.updater.start_polling()


class ProgressMessage:
    def __init__(self, bot, chat_id, message_id):
        self.bot = bot
        self.chat_id = chat_id
        self.message_id = message_id
        self.progress_symbol = '🥕'
        self.msg_head = 'Progress:'
        self.msg_footer = ''
        self.last_sent_time = 0
        self.progressbar_width = 10
        self.update_msg = partial(self.bot.edit_message_text,
                                  chat_id=self.chat_id,
                                  message_id=self.message_id,
                                  disable_web_page_preview=True)

    def render_progress_bar(self, percent: float):
        percent_per_symbol = 100 / self.progressbar_width
        progress = self.progress_symbol * int(percent / percent_per_symbol)
        return f'[{progress:➖<10}]'

    def update_progress(self, percent):
        self.update_msg(text=(f'{self.msg_head}\n\n'
                              f'{self.render_progress_bar(percent)} {percent:.2f}%\n'
                              f'{self.msg_footer}'))
        self.last_sent_time = time()


def init(telegram_token, commands, con_pool_size=8, all_burst_limit=30, all_time_limit_ms=3000) -> MQBot:
    mqueue = mq.MessageQueue(all_burst_limit=all_burst_limit, all_time_limit_ms=all_time_limit_ms)
    request = Request(con_pool_size=con_pool_size)
    bot = MQBot(telegram_token, request=request, mqueue=mqueue)
    bot.register_commands(commands)
    return bot
