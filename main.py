import bot
from commands.base import start
from commands.ffmpeg import youtubedl, youtubejustdl
import settings


commands = (
    bot.Cmd('start', start, None),
    bot.Cmd('g', youtubedl, None),
    bot.Cmd('d', youtubejustdl, None),
)


if __name__ == '__main__':
    token = settings.TG_TOKEN

    mybot = bot.init(telegram_token=token, commands=commands)
    mybot.start_polling()
